package com.example.projectsplashscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Implementing a splash screen
        Handler handler = new Handler();
        Runnable run = new Runnable(){
            @Override
            public void run() {
                finish();
                // implementing an Intent constructor that takes (2) parameters {PkgContext -- cls}
                Intent intent = new Intent(MainActivity.this,ExAcetic.class);
                startActivity(intent);
            }
        };
        //Attaching Handler Action to the timer
        handler.postDelayed(run,3000 );
    }
}